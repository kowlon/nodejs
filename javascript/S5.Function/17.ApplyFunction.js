var person = {
  fullName: function () {
    return this.firstName + ' ' + this.lastName;
  }
};

var person1 = {
  firstName: 'NodeJS',
  lastName: 'Javascript'
};

var x = person.fullName.apply(person1);
console.log(x); //NodeJS
