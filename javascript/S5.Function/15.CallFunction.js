var person = {
  fullName: function () {
    return this.firstName + ' ' + this.lastName;
  }
};
var person1 = {
  firstName: 'NodeJS 11141441',
  lastName: 'Javascript'
};
var person2 = {
  firstName: 'Nikolaj',
  lastName: 'Vestorp'
};
var x = person.fullName.call(person1);
// console.log(x); //NodeJS

// Another Example
function Product(name, price) {
  this.name = name;
  this.price = price;
  console.log(this);
}

function Food(name, price) {
  console.log(this);
  Product.call(this, name, price);
  this.category = 'food';
}

var x = new Food('cheese', 5);

console.log(x.name); // cheese
console.log(x.category); // food

