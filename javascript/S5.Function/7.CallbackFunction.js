function greeting(name) {
  console.log('Hello ' + name);
}

function processUserInput(callback) {
  var name = 'NodeJS';
  callback(name);
}

processUserInput(greeting);
//Hello NodeJS